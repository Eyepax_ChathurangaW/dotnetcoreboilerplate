﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="AlternativeProductDataService.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.DataServices
{
    using System;
    using System.Collections.Generic;
    using IBM.Data.DB2.Core;
    using WISNZ.NZSB.DataServices.Net;
    using WISNZ.NZSB.Entities;
    using WISNZ.NZSB.IDataServices;

    /// <summary>
    /// AlternativeProductDataService implementation.
    /// </summary>
    public class AlternativeProductDataService : IAlternativeProductDataService
    {
        /// <summary>
        /// Get alternative product list.
        /// </summary>
        /// <returns>The list of products.</returns>
        public List<ProductCompact> GetProductAlternativeList()
        {
            List<ProductCompact> info = null;

            AlternativeProductDataServiceHelper test = new AlternativeProductDataServiceHelper();
            test.GetProductAlternativeList();

            return null;
        }

        /// <summary>
        /// Test method.
        /// </summary>
        /// <returns>Returns integer.</returns>
        public int TestMethod()
        {
            AlternativeProductDataServiceHelper test = new AlternativeProductDataServiceHelper();
            test.GetProductAlternativeList();
            return 10;
        }
    }
}
