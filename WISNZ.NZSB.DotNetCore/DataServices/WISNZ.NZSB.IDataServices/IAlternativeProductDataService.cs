﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="IAlternativeProductDataService.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.IDataServices
{
    using System.Collections.Generic;
    using WISNZ.NZSB.Entities;

    /// <summary>
    /// IAlternativeProductDataService interface.
    /// </summary>
    public interface IAlternativeProductDataService
    {
        /// <summary>
        /// Test method.
        /// </summary>
        /// <returns>Returns integer.</returns>
        int TestMethod();

        /// <summary>
        /// Get alternative product list.
        /// </summary>
        /// <returns>The list of products.</returns>
        List<ProductCompact> GetProductAlternativeList();
    }
}
