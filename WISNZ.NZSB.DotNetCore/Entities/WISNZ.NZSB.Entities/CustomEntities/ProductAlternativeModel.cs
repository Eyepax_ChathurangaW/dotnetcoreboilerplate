﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="ProductAlternativeModel.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.Entities.CustomEntities
{
    /// <summary>
    /// Product Alternative object.
    /// </summary>
    public class ProductAlternativeModel
    {
        /// <summary>
        /// Gets or sets the product id.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the product sku.
        /// </summary>
        public string ProductSku { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        public byte Rating { get; set; }

        /// <summary>
        /// Gets or sets the feedback.
        /// </summary>
        public string Feedback { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserId { get; set; }
    }
}
