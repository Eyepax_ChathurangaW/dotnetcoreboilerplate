﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="ProductCompact.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.Entities
{
    /// <summary>
    /// Product object.
    /// </summary>
    public class ProductCompact
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }
    }
}
