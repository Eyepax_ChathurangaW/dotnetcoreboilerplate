﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="WebHttpResponse.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.Entities
{
    /// <summary>
    /// Web http responce object.
    /// </summary>
    /// <typeparam name="T">Gets or sets the web http response object.</typeparam>
    public class WebHttpResponse<T>
    {
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public T Data { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public WebHttpError Error { get; set; }

        /// <summary>
        /// Gets or sets the status code of the HTTP response.
        /// </summary>
        public StatusCode ResponseCode { get; set; }

        /// <summary>
        /// Gets or sets the messages.
        /// </summary>
        /// <value>
        /// The messages.
        /// </value>
        public string Messages { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the response's action status.
        /// </summary>
        /// <value>The action status.</value>
        public bool ActionStatus { get; set; }
    }
}
