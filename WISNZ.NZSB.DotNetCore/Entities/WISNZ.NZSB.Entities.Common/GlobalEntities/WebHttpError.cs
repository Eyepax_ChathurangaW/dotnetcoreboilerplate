﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="WebHttpError.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.Entities
{
    /// <summary>
    /// Web http error object.
    /// </summary>
    public class WebHttpError
    {
    }
}
