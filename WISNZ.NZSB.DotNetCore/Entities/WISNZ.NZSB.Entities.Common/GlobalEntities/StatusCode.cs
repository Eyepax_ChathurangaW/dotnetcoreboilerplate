﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="StatusCode.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.Entities
{
    /// <summary>
    /// Status code Enum.
    /// </summary>
    public enum StatusCode
    {
    }
}
