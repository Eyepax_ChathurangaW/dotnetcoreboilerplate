﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="IAlternativeProductService.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.IServices
{
    /// <summary>
    /// IAlternativeProductService interface.
    /// </summary>
    public interface IAlternativeProductService
    {
        /// <summary>
        /// Test method.
        /// </summary>
        /// <returns>Returns integer.</returns>
        int Testmethod();
    }
}
