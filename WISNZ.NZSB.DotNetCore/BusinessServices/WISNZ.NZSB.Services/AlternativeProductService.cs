﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="AlternativeProductService.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.Services
{
    using WISNZ.NZSB.IDataServices;
    using WISNZ.NZSB.IServices;

    /// <summary>
    /// AlternativeProductService implementation.
    /// </summary>
    public class AlternativeProductService : IAlternativeProductService
    {
        /// <summary>
        /// The alternative product data service.
        /// </summary>
        private readonly IAlternativeProductDataService alternativeProductDataService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlternativeProductService" /> class
        /// </summary>
        /// <param name="alternativeProductDataService">The alternative product data service.</param>
        public AlternativeProductService(IAlternativeProductDataService alternativeProductDataService)
        {
            this.alternativeProductDataService = alternativeProductDataService;
        }

        /// <summary>
        /// Test method.
        /// </summary>
        /// <returns>Returns integer.</returns>
        public int Testmethod()
        {
            return this.alternativeProductDataService.TestMethod();
        }
    }
}
