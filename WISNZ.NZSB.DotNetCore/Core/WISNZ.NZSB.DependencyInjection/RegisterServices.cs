﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="RegisterServices.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.DependencyInjection
{
    using Microsoft.Extensions.DependencyInjection;
    using WISNZ.NZSB.IServices;
    using WISNZ.NZSB.Services;

    /// <summary>
    /// The services dependancy injection.
    /// </summary>
    public class RegisterServices
    {
        /// <summary>
        /// Register services.
        /// </summary>
        /// <param name="services">The .net core services collection.</param>
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IAlternativeProductService, AlternativeProductService>();
        }
    }
}
