﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="RegisterDataServices.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.DependencyInjection
{
    using Microsoft.Extensions.DependencyInjection;
    using WISNZ.NZSB.DataServices;
    using WISNZ.NZSB.IDataServices;

    /// <summary>
    /// The data services dependancy injection.
    /// </summary>
    public class RegisterDataServices
    {
        /// <summary>
        /// Register data services.
        /// </summary>
        /// <param name="services">The .net core services collection.</param>
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IAlternativeProductDataService, AlternativeProductDataService>();
        }
    }
}
