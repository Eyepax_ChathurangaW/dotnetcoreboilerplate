﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="About.cshtml.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.APIServices.Identity.Pages
{
    using Microsoft.AspNetCore.Mvc.RazorPages;

    /// <summary>
    /// The about page model.
    /// </summary>
    public partial class AboutModel : PageModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The starting point of initializing about page.
        /// </summary>
        public void OnGet()
        {
            this.Message = "Your application description page.";
        }
    }
}
