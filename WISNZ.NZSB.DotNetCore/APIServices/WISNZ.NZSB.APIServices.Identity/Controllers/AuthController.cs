﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="AuthController.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.APIServices.Identity.Controllers
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.IdentityModel.Tokens;
    using WISNZ.NZSB.Entities;

    /// <summary>
    /// The authentication controller.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        /// <summary>
        /// The provided APIs for managing user in a persistence store.
        /// </summary>
        private readonly UserManager<IdentityUser> userManager;

        /// <summary>
        /// The provided abstraction for hashing passwords.
        /// </summary>
        private readonly IPasswordHasher<IdentityUser> passwordHasher;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthController" /> class
        /// </summary>
        /// <param name="configuration">The key/value application configuration properties.</param>
        /// <param name="userManager">The provided APIs for managing user in a persistence store.</param>
        /// <param name="passwordHasher">The provided abstraction for hashing passwords.</param>
        public AuthController(
            IConfiguration configuration,
            UserManager<IdentityUser> userManager,
            IPasswordHasher<IdentityUser> passwordHasher)
        {
            this.Configuration = configuration;
            this.userManager = userManager;
            this.passwordHasher = passwordHasher;
        }

        /// <summary>
        /// Gets the key/value application configuration properties.
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// The register user endpoint.
        /// </summary>
        /// <param name="user">The user object.</param>
        /// <returns>The web http response.</returns>
        [HttpPost("RegisterAsync")]
        public async Task<WebHttpResponse<User>> RegisterAsync([FromRoute]User user)
        {
            WebHttpResponse<User> responce = new WebHttpResponse<User>();

            if (!ModelState.IsValid)
            {
                return responce;
            }

            IdentityUser identityUser = new IdentityUser()
            {
                Email = user.Email,
                PasswordHash = user.Password,
                UserName = user.Username
            };

            IdentityResult result = await this.userManager.CreateAsync(identityUser, identityUser.PasswordHash);

            if (result.Succeeded)
            {
                return responce;
            }

            return responce;
        }

        /// <summary>
        /// The create token endpoint.
        /// </summary>
        /// <param name="userModel">The user object.</param>
        /// <returns>The web http response.</returns>
        [HttpPost("CreateTokenAsync")]
        public async Task<WebHttpResponse<string>> CreateTokenAsync([FromRoute]User userModel)
        {
            WebHttpResponse<string> responce = new WebHttpResponse<string>();

            try
            {
                var user = await this.userManager.FindByEmailAsync(userModel.Email);

                if (user == null)
                {
                    return responce;
                }

                if (this.passwordHasher.VerifyHashedPassword(user, user.PasswordHash, userModel.Password) == PasswordVerificationResult.Success)
                {
                    var userClaims = await this.userManager.GetClaimsAsync(user);

                    var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Email, user.Email)
                    }.Union(userClaims);

                    var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.Configuration["JwtSecurityToken:Key"]));
                    var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

                    var jwtSecurityToken = new JwtSecurityToken(
                        issuer: this.Configuration["JwtSecurityToken:Issuer"],
                        audience: this.Configuration["JwtSecurityToken:Audience"],
                        claims: claims,
                        signingCredentials: signingCredentials,
                        expires: DateTime.Now.AddDays(1),
                        notBefore: DateTime.Now.Subtract(TimeSpan.FromMinutes(60)));
                    var token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

                    responce.Data = token;
                }
            }
            catch (Exception ex)
            {
            }

            return responce;
        }
    }
}