﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="AlternativeProductController.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.APIServices.Resources.Controllers
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using WISNZ.NZSB.Entities;
    using WISNZ.NZSB.Entities.CustomEntities;
    using WISNZ.NZSB.IServices;

    /// <summary>
    /// The values controller.
    /// </summary>
    ////[Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class AlternativeProductController : ControllerBase
    {
        /// <summary>
        /// The alternative product service.
        /// </summary>
        private readonly IAlternativeProductService alternativeProductService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AlternativeProductController" /> class
        /// </summary>
        /// <param name="alternativeProductService">The alternative product service.</param>
        public AlternativeProductController(IAlternativeProductService alternativeProductService)
        {
            this.alternativeProductService = alternativeProductService;
        }

        /// <summary>
        /// Get alternative product list.
        /// </summary>
        /// <param name="productAlternative">The product alternative data.</param>
        /// <returns>The list of products.</returns>
        [HttpGet, MapToApiVersion("1.0")]
        [Route("GetProductAlternativeList")]
        public WebHttpResponse<List<ProductCompact>> GetProductAlternativeList(ProductAlternativeModel productAlternative)
        {
            WebHttpResponse<List<ProductCompact>> test = new WebHttpResponse<List<ProductCompact>>();
            return test;
        }

        /// <summary>
        /// Save alternative product rating.
        /// </summary>
        /// <param name="productAlternative">The product alternative data.</param>
        /// <returns>Returns true or false.</returns>
        [HttpPost, MapToApiVersion("1.0")]
        [Route("SaveProductAlternativeRating")]
        public string SaveProductAlternativeRating(ProductAlternativeModel productAlternative)
        {
            return "SaveProductAlternativeRating";
        }

        /// <summary>
        /// Get stock details.
        /// </summary>
        /// <param name="productAlternative">The product alternative data.</param>
        /// <returns>Returns true or false.</returns>
        [HttpGet, MapToApiVersion("1.0")]
        [Route("GetStockDetails")]
        public string GetStockDetails(ProductAlternativeModel productAlternative)
        {
            return "GetStockDetails";
        }

        /// <summary>
        /// The test authorize method.
        /// </summary>
        /// <returns>The test string.</returns>
        [HttpGet, MapToApiVersion("1.0")]
        [Route("Test")]
        public string Get()
        {
            this.alternativeProductService.Testmethod();
            return "test value";
        }
    }
}