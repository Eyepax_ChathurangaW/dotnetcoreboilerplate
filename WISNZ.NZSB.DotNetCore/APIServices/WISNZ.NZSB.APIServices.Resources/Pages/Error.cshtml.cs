#region copyright
// -----------------------------------------------------------------------
// <copyright file="Error.cshtml.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.APIServices.Resources.Pages
{
    using System.Diagnostics;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;

    /// <summary>
    /// The error page model.
    /// </summary>
    public partial class ErrorModel : PageModel
    {
        /// <summary>
        /// Gets or sets the request id..
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets the show request id.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);

        /// <summary>
        /// The starting point of initializing error page.
        /// </summary>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public void OnGet()
        {
            this.RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
        }
    }
}
