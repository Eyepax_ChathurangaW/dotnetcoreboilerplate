﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="Contact.cshtml.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.APIServices.Resources.Pages
{
    using Microsoft.AspNetCore.Mvc.RazorPages;

    /// <summary>
    /// The contact page model.
    /// </summary>
    public partial class ContactModel : PageModel
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The starting point of initializing contact page.
        /// </summary>
        public void OnGet()
        {
            this.Message = "Your contact page.";
        }
    }
}
