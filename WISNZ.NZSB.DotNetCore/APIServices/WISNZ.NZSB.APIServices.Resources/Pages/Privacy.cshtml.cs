﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="Privacy.cshtml.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.APIServices.Resources.Pages
{
    using Microsoft.AspNetCore.Mvc.RazorPages;

    /// <summary>
    /// The privacy page model.
    /// </summary>
    public partial class PrivacyModel : PageModel
    {
        /// <summary>
        /// The starting point of initializing privacy page.
        /// </summary>
        public void OnGet()
        {
        }
    }
}