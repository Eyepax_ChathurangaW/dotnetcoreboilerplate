﻿#region copyright
// -----------------------------------------------------------------------
// <copyright file="Index.cshtml.cs" company="Wesfarmers Industrial & Safety NZ Ltd">
//      Copyright (c) Wesfarmers Industrial & Safety NZ Ltd. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
#endregion

namespace WISNZ.NZSB.APIServices.Resources.Pages
{
    using Microsoft.AspNetCore.Mvc.RazorPages;

    /// <summary>
    /// The index page model.
    /// </summary>
    public partial class IndexModel : PageModel
    {
        /// <summary>
        /// The starting point of initializing index page.
        /// </summary>
        public void OnGet()
        {
        }
    }
}
